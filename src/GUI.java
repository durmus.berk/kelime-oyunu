import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI {
	
	public static void main(String[] args) {
    
		JFrame mainFrame = new JFrame("Kelime Oyunu");
        ImageIcon bg = new ImageIcon("icons//arkaplan.jpg");
          /*Background
          JLabel label = new JLabel(bg);
          mainFrame.add(label, BorderLayout.CENTER);
          */
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(400,600);
		mainFrame.setVisible(true);
        mainFrame.setLocationRelativeTo(null);
		mainFrame.setLayout(new BorderLayout());
		ImageIcon icon = new ImageIcon("icons//icon.png");
	
		mainFrame.setIconImage(icon.getImage());
		infopanel infoPanel = new infopanel();
		centerPanel center = new centerPanel();
		JPanel bottom = new bottomPanel(center,infoPanel);
    
	    mainFrame.add(infoPanel, BorderLayout.NORTH);
	    mainFrame.add(bottom, BorderLayout.SOUTH);
	    mainFrame.add(center, BorderLayout.CENTER);
        
	    mainFrame.setVisible(true);

}

}

